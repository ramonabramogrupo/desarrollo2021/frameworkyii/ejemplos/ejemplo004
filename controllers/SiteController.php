<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionPagina1(){
        return $this->render('pagina1');
    }
    
    public function actionPagina2(){
        return $this->render('pagina',[
            'mensaje' => 'Esta pagina es la numero 2'
        ]);
    }
    
    public function actionPagina3(){
        $frases=[
            "frase 1",
            "frase 2",
            "frase 3"
        ];
        
        $indice=mt_rand(0,count($frases)-1);
        
        return $this->render('pagina',[
            'mensaje' => $frases[$indice]
        ]);
    }
    
    public function actionPagina4(){
        $numeros=[];
             
        
        /**
         * rellenar el array con 10 numeros aleatorios entre 1 y 100
         */
        for($c=0;$c<10;$c++){
            $numeros[$c]= mt_rand(1,100);
        } 
        
        /**
         * concateno todos los elementos del array
         */
        $salida=implode(",", $numeros);
        
        
        return $this->render("pagina",[
           'mensaje' => $salida, 
        ]);
        
    }
    
    public function actionPagina5(){
        return $this->render("dados");
    }
    
    public function actionPagina6(){
        return $this->render("dados_1",[
           "numero" => mt_rand(1,6), 
        ]);
        
    }
    
    public function actionPagina7(){
        if($datos=Yii::$app->request->get()){
            //$operacion=$datos->numero1+$datos->numero2;
            $operacion=$datos["numero1"]+ $datos["numero2"];
            return $this->render("mostrar",[
                "datos" => $datos,
                "resultado" => $operacion,
             ]);
        }else{
            return $this->render("formulario");
        }
    }
    
    public function actionPagina8(){
        
        //cargar los datos que vienen del formulario
        if($datos=Yii::$app->request->get()){
            /*
             * procesamiento de la informacion
             */
            /*
            $resultado["suma"]=$datos["numero1"]+$datos["numero2"]+$datos["numero3"];
            $resultado["resta"]=$datos["numero1"]-$datos["numero2"]-$datos["numero3"];
            $resultado["producto"]=$datos["numero1"]*$datos["numero2"]*$datos["numero3"];
             */
            
            $resultado=[
              "suma" => $datos["numero1"]+$datos["numero2"]+$datos["numero3"],
              "resta" => $datos["numero1"]-$datos["numero2"]-$datos["numero3"],
              "producto" => $datos["numero1"]*$datos["numero2"]*$datos["numero3"],
            ];
            /*
             * mostrar los resultados
             */
            return $this->render("mostrar_1",[
                "datos" => $datos,
                "resultado" => $resultado,
            ]);
        }else{ // aqui se entra cuando cargas el formulario 
            /*
             * introduccion de los datos 
             */
            return $this->render("formulario_1");
        }
        
    }
    
    
    
    
    
    
    
    
    
    public function actionPagina9(){
        if($datos=Yii::$app->request->get()){
            //$operacion=$datos->numero1+$datos->numero2;
            $operacion=$datos["numero1"]+ $datos["numero2"];
            return $this->render("mostrar",[
                "datos" => $datos,
                "resultado" => $operacion,
             ]);
        }else{
            return $this->render("formulario");
        }
    }
    
}

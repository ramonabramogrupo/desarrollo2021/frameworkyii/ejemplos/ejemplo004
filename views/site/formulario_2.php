<?php
    use yii\helpers\Url;
?>
<form method="get" action="<?= Url::to('site/pagina7') ?>">
<?= Html::beginForm(['/', 'id' => $id], 'post') ?>
    <div>
        <label>Numero 1</label>
        <input type="text" name="numero1">
    </div>
    <br>
    <div>
        <label>Numero 2</label>
        <input type="text" name="numero2">
    </div>
    <br>
    <button>Enviar</button>
</form>

